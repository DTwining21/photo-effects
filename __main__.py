from source_code.file_convertions import *
from source_code.color_effects import *
from source_code.global_functions import *
from PIL import Image
import argparse


def main(arguments):
    photo = Image.open("{}{}".format(IMAGE_FOLDER, arguments.image_file_name))

    pencilSketch(photo, arguments.radius)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Example')

    parser.add_argument('image_file_name',
                        type=str,
                        help='Provide the name of an image file.')

    parser.add_argument('--radius',
                        default=21,
                        type=float,
                        help='Provide a radius: 0.0 <= radius <= 1.0.')

    arguments = parser.parse_args()

    main(arguments)
