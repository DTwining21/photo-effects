## @package global_functions
# A set of basic and versitile functions to be used in other modules.

from PIL import Image
from PIL import ImageOps
import os
import ntpath
import math
import random

SAVE_PATH = "altered_images/"
IMAGE_FOLDER = "original_images/"

# define functions that compute weighted averages


def weighted_average(a, b, t):
    """
    Computes the weighted average of 2 numbers.

    :param a: One of the numbers to be averaged
    :type a: int,
    :param b: The other number to be averaged
    :type b: int
    :param t: The weight, 0.0 <= t <= 1.0
    :type t: float

    :return: the weighted average of a and b, with weight t
    """

    return (1 - t) * a + t * b


# end of weighted_average()


def weighted_average_points(a, b, t):
    """
     Compute the weighted average of 2 points.

    :param a: Description of parameter `a`.
    :type a: type
    :param b: Description of parameter `b`.
    :type b: type
    :param t: Description of parameter `t`.
    :type t: type
    :return: Description of returned object.
    :rtype: type

    """

    (x0, y0) = a
    (x1, y1) = b
    x = weighted_average(x0, x1, t)
    y = weighted_average(y0, y1, t)
    return (x, y)


# end of weighted_average_points()


def weighted_average_colors(a, b, t):
    """ Compute the weighted average of 2 colors. """
    (a_red, a_green, a_blue) = a
    (b_red, b_green, b_blue) = b

    red = int(weighted_average(a_red, b_red, t))
    green = int(weighted_average(a_green, b_green, t))
    blue = int(weighted_average(a_blue, b_blue, t))

    return (red, green, blue)


# end of weighted_average_colors()

# define functions that compute normalized coordinates


def normalized_cartesian_coordinates(row, column, width, height):
    """ Compute x and y coordinates where -1.0 <= x, y <= 1.0. """
    half_width = width / 2
    half_height = height / 2

    radius = max(half_width, half_height)

    x = (column - half_width) / radius
    y = (row - half_height) / radius

    return (x, y)


# end of normalized_cartesian_coordinates()


def normalized_polar_coordinates(row, column, width, height):
    # compute polar coordinates: a radius and an angle
    #    0.0 <= radius <= 1.0
    #    0.0 <= angle < 2 * pi
    # radius is a measure of the distance of a pixel
    # from the center of the image
    (x, y) = normalized_cartesian_coordinates(row, column, width, height)

    radius = math.sqrt(x * x + y * y)
    angle = math.atan2(y, x)

    return (radius, angle)


# end of normalized_polar_coordinates()


def openPhoto(path):
    """
    Opens the given photo if not already open.

    Checks to see if a path has been passed. If so, it opens it and returns the open photo. If an open photo has instead been passed, it simply returns the photo.

    :type path: PIL.Image, str
    :param path: Accepts a path to a photo as a string or an open Image object.

    :return: An open Image object containing the specified image file.
    :rtype: PIL.Image
    """
    if isinstance(path, str):
        return Image.open(path)
    else:
        return path


def saveAndRename(path, nameToAppend="", newExtension=""):
    """
    Saves the specified image with the given name changes.

    May change file type if a new extension is given.

    :type path: PIL.Image or str
    :type nameToAppend: str
    :type newExtension: str

    :param path: The Image object or path to the image file to save
    :param nameToAppend: The information string to append to the name of the image file defore saving it, defaults to ""
    :param newExtension: The new extention type to assign to the image file, defaults to ""

    :return: a PIL.Image object that contains the saved image file
    """
    photo = openPhoto(path)
    file = ntpath.basename(photo.filename)

    print("saving file: {}".format(file))

    fileName, fileExtension = os.path.splitext(file)

    if newExtension == "":
        newExtension = fileExtension
    newPath = "{}{}{}{}".format(SAVE_PATH, fileName, nameToAppend,
                                newExtension)
    photo.save(newPath)
    print("saved file at: {}".format(newPath))
    return openPhoto(newPath)


def checkRange(value, lowLimit, highLimit):
    """Short summary.

    :param value: Description of parameter `value`.
    :type value: type
    :param lowLimit: Description of parameter `lowLimit`.
    :type lowLimit: type
    :param highLimit: Description of parameter `highLimit`.
    :type highLimit: type
    :return: Description of returned object.
    :rtype: checkRange
    """
    if isinstance(value, int):
        if lowLimit <= value <= highLimit:
            return True
    else:
        if lowLimit[0] <= value[0] <= highLimit[0]:
            if lowLimit[1] <= value[1] <= highLimit[1]:
                if lowLimit[2] <= value[2] <= highLimit[2]:
                    return True
    return False


def findLower(value1, value2):
    """
    determines and returns the lower of two given values

    :param value1: The first value to compare
    :type value1: int, float
    :param value2: The second value to compare
    :type value2: int, float
    :return: The lower of the two given values
    :rtype: int, float

    """
    if value1 <= value2:
        return value1
    else:
        return value2


def findGreater(value1, value2):
    """Short summary.

    :param value1: Description of parameter `value1`.
    :type value1: type
    :param value2: Description of parameter `value2`.
    :type value2: type
    :return: Description of returned object.
    :rtype: findGreater

    """
    if value1 >= value2:
        return value1
    else:
        return value2


def difference(value1, value2):
    """Short summary.

    :param value1: Description of parameter `value1`.
    :type value1: type
    :param value2: Description of parameter `value2`.
    :type value2: type
    :return: Description of returned object.
    :rtype: difference

    """
    return abs(value1 - value2)


def average(num1, num2, weight1=1, weight2=1):
    """Short summary.

    :param num1: Description of parameter `num1`.
    :type num1: type
    :param num2: Description of parameter `num2`.
    :type num2: type
    :param weight1: Description of parameter `weight1`. Defaults to 1.
    :type weight1: type
    :param weight2: Description of parameter `weight2`. Defaults to 1.
    :type weight2: type
    :return: Description of returned object.
    :rtype: average

    """
    return ((num1 * weight1) + (num2 * weight2)) // 2


def colorAverage(color1, color2, weight1=1, weight2=1):
    """Short summary.

    :param color1: Description of parameter `color1`.
    :type color1: type
    :param color2: Description of parameter `color2`.
    :type color2: type
    :param weight1: Description of parameter `weight1`. Defaults to 1.
    :type weight1: type
    :param weight2: Description of parameter `weight2`. Defaults to 1.
    :type weight2: type
    :return: Description of returned object.
    :rtype: colorAverage

    """
    return (average(color1[0], color2[0], weight1,
                    weight2), average(color1[1], color2[1], weight1, weight2),
            average(color1[2], color2[2], weight1, weight2))


#def distFromLeft ():
#a function that computes the relative distance of a pixel from the left edge of the image (0.0 means on the left edge, 1.0 means on the right edge)

#def distFromBottom ():
#function that computes the relative distance of a pixel from the bottom image of the image

#def distFromCenter ():
#a function that computes the relative distance of a pixel from the center of the image (0.0 means at the center, 1.0 means at one of the corners)


def randomColor(num):
    """Short summary.

    :param num: Description of parameter `num`.
    :type num: type
    :return: Description of returned object.
    :rtype: randomColor

    """
    colorList = []
    for i in range(0, num):
        colorList.append((random.randint(0, 255), random.randint(0, 255),
                          random.randint(0, 255)))
    return colorList


def mixColors(color1, color2, numberOfMixes):
    """Short summary.

    :param color1: Description of parameter `color1`.
    :type color1: type
    :param color2: Description of parameter `color2`.
    :type color2: type
    :param numberOfMixes: Description of parameter `numberOfMixes`.
    :type numberOfMixes: type
    :return: Description of returned object.
    :rtype: type

    """
    colorList = []
    red = []
    green = []
    blue = []

    redIncrement = difference(color1[0], color2[0]) / numberOfMixes
    greenIncrement = difference(color1[1], color2[1]) / numberOfMixes
    blueIncrement = difference(color1[2], color2[2]) / numberOfMixes

    redLesser = min(color1[0], color2[0])
    greenLesser = min(color1[1], color2[1])
    blueLesser = min(color1[2], color2[2])

    for i in range(1, numberOfMixes + 1):
        colorList.append((redLesser + (redIncrement * i)),
                         (greenLesser + (greenIncrement * i)),
                         (blueLesser + (blueIncrement * i)))

    return colorList
