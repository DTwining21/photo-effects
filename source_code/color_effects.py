from PIL import Image
from PIL import ImageFilter
from PIL import ImageOps
from .global_functions import *
from .file_convertions import *
from .Pointillism.main import *
import progressbar
import math
import random
import cv2
import ntpath
import os
import numpy

BLACK_RGB = (255, 255, 255)
WHITE_RGB = (0, 0, 0)


def dodge(path, mask, save=True):
    photo = openPhoto(path)
    mask = openPhoto(mask)
    file = photo.filename

    photo = numpy.asarray(photo)
    mask = numpy.asarray(mask)

    photo = cv2.divide(photo, 255 - mask, scale=256)

    photo = Image.fromarray(photo)

    photo.filename = file
    if save:
        return saveAndRename(photo, "_dodge")
    return photo


def burn(path, mask, save=True):
    photo = openPhoto(path)
    mask = openPhoto(mask)
    file = photo.filename

    photo = numpy.asarray(photo)
    mask = numpy.asarray(mask)

    photo = 255 - cv2.divide(255 - photo, 255 - mask, scale=256)

    photo = Image.fromarray(photo)

    photo.filename = file
    if save:
        return saveAndRename(photo, "_burn")
    return photo


def greyscale(path, strategy="weighted", save=True):
    """
    Converts image to greyscale using one of three methods for different quality levels.

    :param path: A PIL.Image object or a path to an image file to be converterd to greyscale.
    :type path: PIL.Image, str
    :param strategy: The method to use to convert the image to greyscale. Defaults to "weighted".
    :type strategy: str
    :param save: Whether or not to save a copy of the altered image file. Defaults to True.
    :type save: bool
    :return: The altered image.
    :rtype: PIL.Image
    """
    photo = openPhoto(path)
    x, y = photo.size
    strategy = strategy.lower().replace(" ", "")

    print("converting {} to greyscale using {} strategy".format(
        photo.filename, strategy))

    if strategy == "average" or strategy == "averaged":
        for i in range(x):
            for j in range(y):
                color = photo.getpixel((i, j))
                avg = (color[0] + color[1] + color[2]) // 3
                photo.putpixel((i, j), (avg, avg, avg))
    elif strategy == "weighted":
        for i in range(x):
            for j in range(y):
                color = photo.getpixel((i, j))
                avg = int((color[0] * 0.299) + (color[1] * 0.587) +
                          (color[2] * 0.114))
                photo.putpixel((i, j), (avg, avg, avg))
    elif strategy == "halftone":
        pixels = photo.load()

        for i in range(0, x, 2):
            for j in range(0, y, 2):
                p1 = photo.getpixel((i, j))
                p2 = photo.getpixel((i, j + 1))
                p3 = photo.getpixel((i + 1, j))
                p4 = photo.getpixel((i + 1, j + 1))

                grey1 = (p1[0] * 0.299) + (p1[1] * 0.587) + (p1[2] * 0.114)
                grey2 = (p2[0] * 0.299) + (p2[1] * 0.587) + (p2[2] * 0.114)
                grey3 = (p3[0] * 0.299) + (p3[1] * 0.587) + (p3[2] * 0.114)
                grey4 = (p4[0] * 0.299) + (p4[1] * 0.587) + (p4[2] * 0.114)

                sat = (grey1 + grey2 + grey3 + grey4) / 4

                if sat > 223:
                    pixels[i, j] = BLACK_RGB
                    pixels[i, j + 1] = BLACK_RGB
                    pixels[i + 1, j] = BLACK_RGB
                    pixels[i + 1, j + 1] = BLACK_RGB
                elif sat > 159:
                    pixels[i, j] = BLACK_RGB
                    pixels[i, j + 1] = WHITE_RGB
                    pixels[i + 1, j] = BLACK_RGB
                    pixels[i + 1, j + 1] = BLACK_RGB
                elif sat > 95:
                    pixels[i, j] = BLACK_RGB
                    pixels[i, j + 1] = WHITE_RGB
                    pixels[i + 1, j] = WHITE_RGB
                    pixels[i + 1, j + 1] = BLACK_RGB
                elif sat > 32:
                    pixels[i, j] = WHITE_RGB
                    pixels[i, j + 1] = BLACK_RGB
                    pixels[i + 1, j] = WHITE_RGB
                    pixels[i + 1, j + 1] = WHITE_RGB
                else:
                    pixels[i, j] = WHITE_RGB
                    pixels[i, j + 1] = WHITE_RGB
                    pixels[i + 1, j] = WHITE_RGB
                    pixels[i + 1, j + 1] = WHITE_RGB
    else:
        print(
            "Greyscale strategy not supported. Try \"average\", \"averaged\", \"weighted\", or \"halftone\""
        )
        return photo

    print("greyscale convertion finished")

    if save:
        return saveAndRename(photo, "_greyscale-{}".format(strategy))
    return photo


def gaussianBlur(path, radius=2, save=True):
    photo = openPhoto(path)
    file = photo.filename

    photo = photo.filter(ImageFilter.GaussianBlur(radius))

    photo.filename = file
    if save:
        return saveAndRename(photo, "_GaussianBlur")
    return photo


def filterImage(path, filter, save=True):
    """
    Filters the given image with the specified image and saves a copy.

    :param path: A PIL.Image object or a path to an image file to be converterd to greyscale.
    :type path: PIL.Image, str
    :param filter: The filter to use on the photo.
    :type filter: str
    :param save: Whether or not to save a copy of the image. Defaults to True.
    :type save: bool
    :return: The altered image object.
    :rtype: PIL.Image

    """
    photo = openPhoto(path)
    file = photo.filename
    filter = filter.upper().replace(" ", "_")

    if filter == "BLUR":
        photo = photo.filter(ImageFilter.BLUR)
    elif filter == "CONTOUR":
        photo = photo.filter(ImageFilter.CONTOUR)
    elif filter == "DETAIL":
        photo = photo.filter(ImageFilter.DETAIL)
    elif filter == "EDGE_ENHANCE":
        photo = photo.filter(ImageFilter.EDGE_ENHANCE)
    elif filter == "EDGE_ENHANCE_MORE":
        photo = photo.filter(ImageFilter.EDGE_ENHANCE_MORE)
    elif filter == "EMBOSS":
        photo = photo.filter(ImageFilter.EMBOSS)
    elif filter == "FIND_EDGES":
        photo = photo.filter(ImageFilter.FIND_EDGES)
    elif filter == "SHARPEN":
        photo = photo.filter(ImageFilter.SHARPEN)
    elif filter == "SMOOTH":
        photo = photo.filter(ImageFilter.SMOOTH)
    elif filter == "SMOOTH_MORE":
        photo = photo.filter(ImageFilter.SMOOTH_MORE)
    elif filter == "INVERT":
        photo = ImageOps.invert(photo)

    photo.filename = file
    if save:
        return saveAndRename(photo, "_filter-{}".format(filter))
    return photo


def pointilistPainting(path,
                       stroke_scale=0,
                       gradient_smoothing_radius=0,
                       palette_size=20,
                       limit_image_size=0,
                       save=True):
    """
    Creates a pointillist painting inspired style version of the given image from a customized external opensource package and saves it.

    :param path: A PIL.Image object or a path to an image file to be converterd to into a pointillist painting.
    :type path: PIL.Image, str
    :param stroke_scale: Scale of the brush strokes in pixels (0 = automatic). Defaults to 0.
    :type stroke_scale: int
    :param gradient_smoothing_radius: Radius of the smooth filter applied to the gradient in pixels (0 = automatic). Defaults to 0.
    :type gradient_smoothing_radius: int
    :param palette_size: Number of colors of the base palette. Defaults to 20.
    :type palette_size: int
    :param limit_image_size: Image size limit in pixels (0 = no limits). Defaults to 0.
    :type limit_image_size: int
    :param save: Whether or not to save a copy of the image. Defaults to True.
    :type save: bool
    :return: The altered Image object
    :rtype: PIL.Image

    """

    photo = openPhoto(path)
    newFileName = pointillismMain(photo.filename)
    photo = openPhoto(newFileName)

    if save:
        return saveAndRename(photo, "_pointillist")
    return photo


def blendPhotos(path1, path2, radius=0.1, transition=1.0, save=True):
    photo1 = convertToPng(openPhoto(path1))
    photo2 = convertToPng(openPhoto(path2))

    (width1, height1) = photo1.size
    (width2, height2) = photo2.size

    width = min(width1, width2)
    height = min(height1, height2)

    photo1 = resize(photo1, width, height, False)
    photo2 = resize(photo2, width, height, False)

    print("creating composite photo, please wait.")
    photo = Image.new("RGB", (width, height))

    bar = progressbar.ProgressBar()
    for column in bar(range(0, width)):
        for row in range(0, height):
            (distance,
             angle) = normalized_polar_coordinates(row, column, width, height)

            if distance < radius:
                c = photo1.getpixel((column, row))
                photo.putpixel((column, row), c)
            else:
                a = photo1.getpixel((column, row))
                b = photo2.getpixel((column, row))
                t = (distance - radius) / (1.0 - radius)
                t = math.pow(t, transition)
                c = weighted_average_colors(a, b, t)
                photo.putpixel((column, row), c)

    name1 = os.path.splitext(ntpath.basename(photo1.filename))
    name2 = os.path.splitext(ntpath.basename(photo2.filename))
    photo.filename = "({})blended-with({}).png".format(name1[0], name2[0])

    if save:
        return saveAndRename(photo)
    return photo


def pencilSketch(path, radius=21, save=True, stepSave=False):
    photo = openPhoto(path)
    file = photo.filename

    photo = greyscale(photo, save=stepSave)
    photoCopy = filterImage(photo, "INVERT", stepSave)
    photoCopy = gaussianBlur(photoCopy, radius, save=stepSave)
    photo = dodge(photo, photoCopy, False)

    photo.filename = file
    if save:
        return saveAndRename(photo, "_pencilSketchFilter")
    return photo


def paintingInProgressFilter(path, radius=0.1, transition=1.0, save=True):
    photo = convertToPng(openPhoto(path))
    color_photo = pointilistPainting(photo)
    grayscale_photo = pencilSketch(photo)

    file = photo.filename
    photo = blendPhotos(color_photo, grayscale_photo, radius, transition,
                        False)

    photo.filename = file
    if save:
        return saveAndRename(photo, "_paintingInProgressFilter")
    return photo
