from PIL import Image
from .global_functions import *
import os
import ntpath


def thumbnail(path, max_x, max_y, save=True):
    photo = openPhoto(path)
    fileName = photo.filename

    print("creating thumbnail of {}".format(fileName))

    newSize = (max_x, max_y)
    photo = photo.thumbnail(newSize)
    photo.filename = fileName
    (x, y) = photo.size

    print("image resized to {} x {} thumbnail".format(x, y))

    if save:
        return saveAndRename(photo, "_t{}x{}".format(x, y))
    return photo


def resize(path, x, y, save=True):
    """
    Resizes the specified image to given parameters. Saves a copy of resized image if not stated otherwise.

    :param path: PIL.Image object or path to image file to resize
    :type path: PIL.Image, str
    :param x: New image width in pixels
    :type x: int
    :param y: New image height in pixels
    :type y: int
    :param save: Whether or not to save a copy of the image. Defaults to True.
    :type save: bool

    :return: The resized Image object
    :rtype: PIL.Image
    """
    photo = openPhoto(path)
    fileName = photo.filename

    print("resizing {}".format(fileName))

    newSize = (x, y)
    photo = photo.resize(newSize)
    photo.filename = fileName

    print("image resized to {} x {}".format(x, y))

    if save:
        return saveAndRename(photo, "_{}x{}".format(x, y))
    return photo


def rotate(path, degrees, save=True):
    """
    Rotates the image by the specified number of degrees

    :param path: PIL.Image object or path to image file to rotate.
    :type path: PIL.Image, str
    :param degrees: The number of degrees to rotate the image.
    :type degrees: int
    :param save: Whether or not to save a copy of the image. Defaults to True.
    :type save: bool
    :return: The rotated Image object
    :rtype: PIL.Image
    """
    photo = openPhoto(path)
    fileName = photo.filename

    print("rotating {}".format(fileName))

    photo = photo.rotate(degrees, Image.BICUBIC, True)
    photo.filename = fileName

    print("image rotated by {}".format(degrees))
    if save:
        return saveAndRename(photo, "_rot{}deg".format(degrees))
    return photo


def convertFileType(path, newExtension):
    """
    Converts the image file from one file type to another and saves it.

    :param path: PIL.Image object or path to image file to convert.
    :type path: PIL.Image, str
    :param newExtension: The new extention type to assign to the image file.
    :type newExtension: str
    :return: The resized Image object
    :rtype: PIL.Image
    """
    photo = openPhoto(path)
    file = ntpath.basename(photo.filename)
    fileName, oldExtension = os.path.splitext(file)

    if oldExtension != newExtension:
        print("converting {} from {} to {}".format(fileName, oldExtension,
                                                   newExtension))
        return saveAndRename(photo, "", newExtension)
    else:
        print("No format change needed for {}".format(fileName))
        return photo


#def convertColorFormat(path):
#detect and convert between RGB and HSV


def convertToPng(path):
    """
    C

    :param path: PIL.Image object or path to image file to convert
    :type path: PIL.Image, str
    :return: Description of returned object.
    :rtype: type

    """
    return convertFileType(path, ".png")


def converToJpg(path):
    """Short summary.

    :param path: PIL.Image object or path to image file to convert
    :type path: PIL.Image, str
    :return: Description of returned object.
    :rtype: type

    """
    return convertFileType(path, ".jpg")


def convertAllToPng(directory):
    """Short summary.

    :param directory: Description of parameter `directory`.
    :type directory: type
    :return: Description of returned object.
    :rtype: type

    """
    print("begining to convery all files in directory {}".format(directory))

    i = 0
    for file in os.listdir(directory):
        convertToPng("{}/{}".format(directory, file))
        i = i + 1

    print("all {} files converted".format(i))


def convertAllToJpg(directory):
    """Short summary.

    :param directory: Description of parameter `directory`.
    :type directory: type
    :return: Description of returned object.
    :rtype: type

    """
    print("begining to convery all files in directory {}".format(directory))

    i = 0
    for file in os.listdir(directory):
        convertToJpg("{}/{}".format(directory, file))
        i = i + 1

    print("all {} files converted".format(i))


# define functions that change dimensions of an image


def make_image_square(path, save=True):
    photo = openPhoto(path)
    fileName = photo.filename

    print("making {} square".format(fileName))

    # get the current dimensions of the image
    (width, height) = photo.size

    # crop to get a square image
    if width > height:
        # compute number of pixels to be cut off
        # at left and right of image
        margin = (width - height) // 2

        # coordinates of upper left corner
        # of region that will be retained
        ulx = margin
        uly = 0

        # coordinates of lower right corner
        # of region that will be retained
        lrx = width - margin
        lry = height
        photo = photo.crop((ulx, uly, lrx, lry))
    else:
        # compute number of pixels to be cut off
        # at top and bottom of image
        margin = (height - width) // 2

        # coordinates of upper left corner
        # of region that will be retained
        ulx = 0
        uly = margin

        # coordinates of lower right corner
        # of region that will be retained
        lrx = width
        lry = height - margin
        photo = photo.crop((ulx, uly, lrx, lry))

    photo.filename = fileName

    print("image made square")

    if save:
        return saveAndRename(photo, "_makeSquare")
    return photo


# end of make_image_square()


def halve_size_of_image(path, save=True):
    photo = openPhoto(path)
    fileName = photo.filename

    print("halving {}".format(fileName))

    (width, height) = photo.size
    width = width // 2
    height = height // 2

    photo = ImageOps.fit(photo, (width, height))

    photo.filename = fileName

    print("image halved")

    if save:
        return saveAndRename(photo, "_halved")
    return photo


# end of halve_size_of_image()
